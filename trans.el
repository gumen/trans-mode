;; -*- lexical-binding: t -*-
;;; trans
(defvar trans-buffer-name "*Trans*" "Name of trans output buffer")
(defvar trans-minibuffer-keymap (copy-keymap minibuffer-local-map) "trans minibuffer keymap")
(defvar trans-mode-map (make-sparse-keymap) "trans-mode keymap")
(defvar trans-history nil "trans commands history")
(defvar trans-command-template "-no-auto pl:en \"\"" "trans command template")

(define-derived-mode trans-mode special-mode "Trans"
  "Major mode for `trans' (google-translate) output")

(defun trans-flip-source-target ()
  "Change source to target and target to source language in
minibuffer."
  (interactive)
  (save-excursion
    (move-beginning-of-line 1)
    (when (search-forward-regexp "[a-z]\\{2\\}:[a-z]\\{2\\}" nil t)
      (backward-word 1)
      (transpose-words 1))))

(defun trans (command)
  "Interface to trans `google-translate-cli' shell program.
Run `trans' program with provided COMMAND and display output in
`*Trans*' buffer."
  (interactive
   (list (read-from-minibuffer
          "trans "
          `(,trans-command-template . ,(length trans-command-template))
          trans-minibuffer-keymap nil 'trans-history)))
  (when (get-buffer trans-buffer-name)
    (let ((buffer-window (get-buffer-window trans-buffer-name)))
      (if buffer-window (quit-window nil buffer-window)))
    (kill-buffer trans-buffer-name))
  (let ((buffer (get-buffer-create trans-buffer-name)))
    ;; Avoid showing short outputs in echo area by opening *Trans*
    ;; buffer manually before command is executed.
    (pop-to-buffer buffer)
    (shell-command (concat "trans " command) buffer))
  (let ((start (search-forward-regexp "\n\n" nil t))
        (end (search-forward-regexp "\n\n" nil t)))
    (when (and start end)
      (kill-ring-save start (- end 2))
      (message "Saved in kill ring: `%s'" (car kill-ring))
      (put-text-property start end 'face "bold")))
  (trans-mode)
  (setq-local truncate-lines nil)
  (setq-local word-wrap t))

(define-key trans-minibuffer-keymap (kbd "C-c C-f") 'trans-flip-source-target)
(define-key trans-minibuffer-keymap (kbd "<backtab>") 'trans-flip-source-target)
(define-key trans-mode-map (kbd "q") 'quit-window)

(provide 'trans)
